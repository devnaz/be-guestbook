from django.contrib import admin
from .models import *

class PostAdmin(admin.ModelAdmin):
    exclude = ['date', 'email']

admin.site.register(Post, PostAdmin)
