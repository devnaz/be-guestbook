from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from rest_framework import generics
import json
from django.core import serializers

from .models import Post
from .forms import PostForm
from .serializers import PostSerializer


def index(request):
    all_posts = Post.objects.filter(active=True).order_by('-date')
    paginator = Paginator(all_posts, 50)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    form = PostForm()
    return render(request, 'guestbook/index.html', {'posts': posts,
                                                    'form': form})


def add_post(request):
    posts = Post.objects.filter(active=True)
    if request.POST:
        form = PostForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            # do something before save form
            instance.save()
            form = PostForm() # clean form
            return HttpResponseRedirect(reverse('index'))
        else:
            form = PostForm(request.POST or None)
            return render(request, 'guestbook/index.html', {'posts': posts,
                                                            'form': form})


# these 2 functions return result in json and xml formats
def post_json(request):
    queryset = Post.objects.all()
    serializer = PostSerializer(queryset, many=True)
    return HttpResponse(
        json.dumps({
            'posts': serializer.data,
        }),
        content_type='application/json'
    )


def post_xml(request):
    posts = Post.objects.all()
    data = serializers.serialize('xml', posts)
    return HttpResponse(data, content_type="application/xml")


# these 2 functions return result in rest api framework
class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class PostDetail(generics.RetrieveDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
