from django.db import models

class Post(models.Model):
    name = models.CharField(max_length=50)
    text = models.TextField()
    email = models.EmailField(blank=True)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Відгук'
        verbose_name_plural = 'Відгуки'
