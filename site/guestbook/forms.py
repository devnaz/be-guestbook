from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['name', 'email', 'text']
        labels = {
            'name': '*Ваше імя',
            'email': 'Ваш емейл (не обовязково!)',
            'text': '*Ваш відгук',
        }

    def clean_name(self):
        name = self.cleaned_data['name']
        if len(name) < 2:
            raise forms.ValidationError('Імя повинно мати більше 1-го символа!')
        return name

    def clean_text(self):
        name = self.cleaned_data['text']
        if len(name) < 2:
            raise forms.ValidationError('Відгук повинин мати більше 1-го символа!')
        return name
