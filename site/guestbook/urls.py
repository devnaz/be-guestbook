from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add-post/$', views.add_post, name='add_post'),

    url(r'^api/$', views.PostList.as_view()),
    url(r'^api/(?P<pk>[0-9]+)/$', views.PostDetail.as_view()),

    url(r'^post/json$', views.post_json),
    url(r'^post/xml$', views.post_xml),
]

urlpatterns = format_suffix_patterns(urlpatterns)
